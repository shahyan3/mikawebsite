from django import forms
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
# User = get_user_model()


from .models import *
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from django.contrib.auth.forms import UserCreationForm

# Create a custom form that inherites form UserCreationForm (adding our own fields to save i db)
# Inheriting form in the paramters ()
# class RegistrationForm(UserCreationForm): 
# 	email = forms.EmailField(required=True)

# 	class Meta:
# 		model = Accounts
# 		fields = (
# 			'username',
# 			'first_name',
# 			'last_name',
# 			'email',
# 			'password1',
# 			'password2',
 
#  		)	

# 	def save(self, commit=True):
# 		user = super(RegistrationForm, self).save(commit=False)
# 		user.first_name = self.cleaned_data['first_name']
# 		user.last_name = self.cleaned_data['last_name']
# 		user.email = self.cleaned_data['email']

# 		if commit:
# 			user.save()

# 		return user

#  Inherits from UserChangeForm class - we keep everything i.e. methods, functionality same but change the things we want to show - connected to the User model 
class EditProfileForm(UserChangeForm):

	class Meta:
		model = get_user_model()
		# Create fields variable get has all the fields we want to show 
		fields = (
			'email',
			'first_name',
			'last_name',
			'password'
		)


class StudentResistrationForm(forms.ModelForm):
	class Meta:
	    model = StudentProfile
	    fields = (	
	    	'first_name',
	    	'last_name',
			'AMEB_Ratings',
			'birth_date',
			'is_student',
 
	    )


	def save(self, commit=True):
		user = super(StudentResistrationForm, self).save(commit=False)
		# user.first_name = self.cleaned_data['first_name']
		# user.last_name = self.cleaned_data['last_name']
		user.AMEB_Ratings = self.cleaned_data['AMEB_Ratings']

		if commit:
			user.save()

		return user

	def clean_is_student(self):
		is_student = self.cleaned_data.get('is_student')
		if not is_student:
			raise forms.ValidationError('This field is required')
		return is_student

 

class TeacherRegistrationForm(forms.ModelForm):
	class Meta:
	    model = TeacherApplications
	    fields = (
	    	'first_name',
	    	'last_name',
	    	'skill',
	    	'experience_in_years',
	    	'birth_date',
	    	'is_teacher',
	    )
	def clean_is_teacher(self):
		is_teacher = self.cleaned_data.get('is_teacher')
		if not is_teacher:
			raise forms.ValidationError('This field is required')
		return is_teacher


class UserForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'password')
