

from django.db import models
from django.contrib.auth.models import AbstractUser


from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

# User = get_user_model()


from django.db.models.signals import post_save

from django.db.models.signals import *
from django.conf import settings


# class userProfile(models.Model):

#     userName = models.OneToOneField(User)
#     city = models.CharField(max_length=100)

#     def __unicode__(self):  # __str__
#         return unicode(self.userName)



# #  Create a UserProfileManager

# # Create your models here.
# class UserStudentProfile(models.Model):
# 	user = models.OneToOneField(User, on_delete=models.CASCADE)
# 	preferredInstrument = models.CharField(max_length=100, default="none")
# 	age = models.PositiveIntegerField(default=0)

# 	# The admin page shows username (easy to read)
# 	def __str__(self):
# 		return self.user.username

# def create_profile(sender, **kwargs):
# 	if kwargs['created']:
# 		user_profile = UserStudentProfile.objects.create(user=kwargs['instance'])

# post_save.connect(create_profile, sender=User)

# https://www.youtube.com/watch?v=zg75OzTVOnU queries sets



class Accounts(AbstractUser):
    def __str__(self):
       return self.first_name + " " + self.last_name

    email = models.EmailField('email address', unique=True)
    first_name = models.CharField('first name', max_length=30, blank=True)
    last_name = models.CharField('last name', max_length=30, blank=True)
    date_joined = models.DateTimeField('date joined', auto_now_add=True)


class TeacherApplications(models.Model):
    # def __str__(self):
    #    return self.first_name + " " + self.last_name

    user = models.OneToOneField('Accounts', related_name='teacher_profile')
    # additional fields for teachers
    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    instrument = models.CharField(max_length=500, blank=True)
    skill = models.CharField(max_length=30, blank=True)
    experience_in_years = models.PositiveIntegerField(blank=True)

    birth_date = models.DateField(null=True, blank=True)    
    is_teacher = models.BooleanField('teacher status', default=False)


class MusicClass(models.Model):
    def __str__(self):
       return self.class_name    

    teacher = models.ForeignKey('TeacherProfile', related_name='music_class_teacher', null=True)
    class_name = models.CharField(max_length=500, blank=True)
    instrument_taught = models.CharField(max_length=500, blank=True)
    class_level = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)    



class StudentProfile(models.Model):
    def __str__(self):
       return self.first_name + " " + self.last_name

    user = models.OneToOneField('Accounts', related_name='student_profile')
    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    # additional fields for students
    AMEB_Ratings = models.PositiveIntegerField(default=0)

    birth_date = models.DateField(null=True, blank=True)
    student_class = models.ForeignKey(to=MusicClass, related_name="student_class", null=True, blank=True)
    is_student = models.BooleanField('student status', default=False)

class TeacherProfile(models.Model):
    # def __str__(self):
    #    return self.user

    user = models.OneToOneField('TeacherApplications', related_name='hired_teacher_profile', null=True)
    # teacher_class = models.ForeignKey(to=MusicClass, related_name="teacher_class", null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)    



