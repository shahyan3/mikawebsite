from django.shortcuts import render

from django.shortcuts import redirect

from accounts.forms import StudentResistrationForm, TeacherRegistrationForm, EditProfileForm, UserForm, TeacherRegistrationForm

from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


from accounts.models import Accounts, StudentProfile, TeacherApplications

from django.contrib.auth.forms import UserChangeForm

from django.http import HttpResponse

from django.contrib.auth.decorators import login_required


# Create your views here.
# def login(request):
# 		return render(request, 'accounts/login.html')

def home(request):
	return render(request, 'accounts/home.html')

def login_redirect(request):
	return redirect('/login/')

# def register(request):
# 	# Once register page loads, either it will send to the server POST data (if the form is submitted), else if it don't send post data create a user form to register
# 	if request.method == "POST":
# 		form = RegistrationForm(request.POST)
# 		if form.is_valid():
# 			bio = form.cleaned_data['bio']
# 			location = form.cleaned_data['location']
# 			birth_date = form.cleaned_data['birth_date']
# 			form.save()
# 			# Students.objects.create(bio=bio, location=location, birth_date=birth_date)
# 			# Students.objects.create(user=user)
# 			return redirect('../home/')
# 	else:
# 		#  Create the django default user form and send it as a dictionary in args to the reg_form.html page.
# 		form = RegistrationForm()
# 	args = {'form': form}
# 	return render(request, 'accounts/reg_form.html', args)

@login_required	
def view_profile(request):
 	args = {'user': request.user}
 	return render(request, 'accounts/profile.html', args)
 	
@login_required	
def edit_profile(request):
 	 # Handle post request - if the user submits a form change form details and pass the intance user
 	if request.method == 'POST':
 		form = EditProfileForm(request.POST, instance=request.user)

 		if form.is_valid():
 			form.save()
 			return redirect('../profile')
  	# Handles the get request - if no post info is submitted then get the form and display it on the edit profile page. 
 	else:
 		form = EditProfileForm(instance=request.user)
 	args = {'form': form}
 	return render(request, 'accounts/profile_edit.html', args)

def registerStudent(request):
    # Once register page loads, either it will send to the server POST data (if the form is submitted), else if it don't send post data create a user form to register
    if request.method == "POST":
        user_form = UserForm(request.POST)
        form = StudentResistrationForm(request.POST)

        if form.is_valid() and user_form.is_valid():
            User = get_user_model()
            username = user_form.cleaned_data['username']
            email = user_form.cleaned_data['email']
            password = user_form.cleaned_data['password']
            new_user = User.objects.create_user(username=username, email=email, password=password)

            student = form.save(commit=False)
            student.user = new_user
            student.save()

            # Student_profile = StudentProfile()
            # Student_profile.user = new_user
            # Student_profile.AMEB_Ratings = request.POST['AMEB_Ratings']
            # Student_profile.is_student = request.POST.get('is_student', False)

            new_user.save()
            # Student_profile.save()
            # form.save()

            return redirect('/')
    else:
        #  Create the django default user form and send it as a dictionary in args to the reg_form.html page.
        user_form = UserForm()
        form = StudentResistrationForm()        

        # args = {'form_student': form, 'user_form': user_form }
    return render(request, 'accounts/reg_form_students.html', {'form_student': form, 'user_form': user_form })

def teacherApplication(request):
    # Once register page loads, either it will send to the server POST data (if the form is submitted), else if it don't send post data create a user form to register
    if request.method == "POST":
        user_form = UserForm(request.POST)
        form = TeacherRegistrationForm(request.POST)

        if form.is_valid() and user_form.is_valid():
            User = get_user_model()
            username = user_form.cleaned_data['username']
            email = user_form.cleaned_data['email']
            password = user_form.cleaned_data['password']
            new_user = User.objects.create_user(username=username, email=email, password=password)

            teacher = form.save(commit=False)
            teacher.user = new_user
            teacher.save()
 
            # Teacher_profile = TeacherApplications()
            # Teacher_profile.user = new_user
            # Teacher_profile.instrument = request.POST['instrument']
            # Teacher_profile.skill = request.POST['skill']
            # Teacher_profile.experience_in_years = request.POST['experience_in_years']
            # Teacher_profile.is_teacher = request.POST.get('is_teacher', False)

            new_user.save()
            # Teacher_profile.save()
            # form.save()

            return redirect('/')
    else:
        #  Create the django default user form and send it as a dictionary in args to the reg_form.html page.
        user_form = UserForm()
        form = TeacherRegistrationForm()  
    return render(request, 'accounts/reg_form_teachers.html', {'form_student': form, 'user_form': user_form })

 	# https://stackoverflow.com/questions/25503926/how-to-create-abstractuser