from django.contrib import admin

from accounts.models import StudentProfile, TeacherApplications, MusicClass, StudentProfile, TeacherProfile

# #  Customize admin header - doesn't work now since we have a base.html for admin custom
admin.site.site_header = 'Admin Dashboard'

# # This is my admin class which is specifically related to the user profile model (inherits model.admin (admin database table))
class UserProfileAdmin(admin.ModelAdmin):
	# list_display = ('location', 'birth_date','bio')

	#   @param returns self : All methods in class as a self parameter (Because they all need to have the ability to pass objects through them), 
	# 	@param returns obj : every row is represerented by the object paramter. : NOTE: each row in the column admin represents particular object
	
	# 	Display each objects description  
	def user_info(self, obj):
		return obj.description

	# Quering the rows based on something - queryset is a method that admin model inherites - we overiding it. Order by status, if can't then order by User -status would order by descending
	def get_queryset(self, request):
		queryset = super(UserProfileAdmin, self).get_queryset(request)
		updatedQuery = queryset.order_by('birth_date') 
		return updatedQuery

	#  Display short name for user info field 'info'
	user_info.short_description = 'Info'

# # Register your models here - ADD UserProfile in UserProfileAdmin class (our custom admin page )
# admin.site.register(Accounts, UserProfileAdmin)

admin.site.register(StudentProfile, UserProfileAdmin)

admin.site.register(TeacherApplications, UserProfileAdmin)
	
admin.site.register(MusicClass, UserProfileAdmin)

admin.site.register(TeacherProfile, UserProfileAdmin)