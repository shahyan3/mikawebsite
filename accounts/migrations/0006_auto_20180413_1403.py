# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-13 04:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_auto_20180413_1402'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='musicclass',
            name='birth_date',
        ),
        migrations.RemoveField(
            model_name='teacherprofile',
            name='birth_date',
        ),
    ]
