# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-13 04:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20180413_1358'),
    ]

    operations = [
        migrations.AddField(
            model_name='musicclass',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='musicclass',
            name='teacher',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='music_class_teacher', to='accounts.TeacherProfile'),
        ),
    ]
